[when] Enc.Time - Evaluated Person had {ENCTYPE:ENUM:EncounterTypeConcept.openCdsConceptCode} whose {HIGHLOW:ENUM:TimeInterval.highLowUpper} time was in the past {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$EncTimeDsl_encounterConcept_{ENCTYPE}{INT}{TIMEUNITS} : EncounterTypeConcept
	(
	openCdsConceptCode == "{ENCTYPE}"
	) and 
	
	$EncTimeDsl_encounterEvent_{ENCTYPE}{INT}{TIMEUNITS} : EncounterEvent
	(
	id == $EncTimeDsl_encounterConcept_{ENCTYPE}{INT}{TIMEUNITS}.conceptTargetId, 
	evaluatedPersonId == $evaluatedPersonId, 
	eval(timeBeforeByAtMost(encounterEventTime.get{HIGHLOW}(), $evalTime, {INT}, {TIMEUNITS}, namedObjects))	
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($EncTimeDsl_encounterEvent_{ENCTYPE}{INT}{TIMEUNITS})))	
) //DslUsed==Enc.Time.Dsl|||ENCTYPE=={ENCTYPE}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}