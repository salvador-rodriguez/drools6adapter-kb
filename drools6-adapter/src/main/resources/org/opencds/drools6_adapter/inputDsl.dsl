[when] Init.EvalTime.FpId.EvalpId - Note that all criteria below must be met for the rule to fire. = 
( 
EvalTime($evalTime : evalTimeValue) and FocalPersonId($focalPersonId : id) and FocalPersonId($evaluatedPersonId : id) 
) //DslUsed==Init.EvalTime.FpId.EvalpId.Dsl